/*импорт компонентов*/
import React from 'react';
import {View, Image, TouchableOpacity, Text, StyleSheet} from 'react-native';

import constants from '../constants/constants';

export default class CheckBox extends React.Component {
    constructor(props) {
        super(props);

        /*состояние класса, isChecked - определяет активность компонента checkBox*/
        this.state = {
            isChecked: false
        }
    }

    /*метод для изменения состояния isChecked*/
    changeIsChecked(isChecked) {
        this.setState({isChecked: !isChecked});
    }

    /*обязательный метод для рендеринга*/
    render() {
        let {text, isChecked, onPress, type} = this.props;
        let {checkBoxChecked, checkBoxUnchecked} = constants.images;
        return(
            <TouchableOpacity
                style={styles.mainContainer}
                onPress={() => onPress(type, !isChecked)}
            >
                <View style={styles.imageContainer}>
                    <Image style={styles.image} source={isChecked ? checkBoxChecked : checkBoxUnchecked}/>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.text}>{text}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

/*CSS-подобные стили для компонентов*/
const styles = StyleSheet.create({
    mainContainer: {
        height: 30,
        flexDirection: 'row',
        alignItems: 'center'
    },
    imageContainer: {
        paddingHorizontal: 10
    },
    image: {
        height: 20,
        resizeMode: 'contain',
        tintColor: 'rgb(255, 140, 0)'
    },
    textContainer: {
        paddingRight: 10
    },
    text: {
        fontSize: 15,
        color: 'rgb(255, 140, 0)',
        fontWeight: 'bold'
    }
});
