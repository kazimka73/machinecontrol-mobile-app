/*импорт компонентов*/
import React from 'react';
import {View, StatusBar} from "react-native";

const StatusBarView = () => (
    <View>

        {/*переопределение стандартной строки состояния устройства*/}
        <StatusBar
            backgroundColor={'#90a4ae'}
            barStyle="light-content"
        />
    </View>
);

export default StatusBarView;