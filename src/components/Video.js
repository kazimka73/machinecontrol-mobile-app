/*импорт компонентов*/
import React from 'react';
import {View, StyleSheet} from 'react-native';
import {NoFlickerImage} from 'react-native-no-flicker-image';


export default class Video extends React.Component {
    constructor(props) {
        super(props);
    }

    /*обязательный компонент для рендеринга*/
    render() {
        let {base64} = this.props;
        return (
            <View style={styles.imageView}>
                {/*сторонний компонент для показа изображений без мерцаний при их частой смене*/}
                <NoFlickerImage
                    style={styles.image}
                    source={{uri: `data:image/jpeg;base64,${base64}`}}
                    resizeMode={"cover"}
                />
            </View>
        );
    }
};

/*CSS-подобные стили для компонентов*/
const styles = StyleSheet.create({
    imageView: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    },
    image: {
        width: '100%',
        height: '100%'
    }
});
