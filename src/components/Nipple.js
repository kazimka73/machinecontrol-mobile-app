/*импорт компонентов*/
import React from 'react';
import {Animated, View, StyleSheet, PanResponder} from 'react-native';

export default class Nipple extends React.Component {
    constructor(props) {
        super(props);
        /*параметры компонента от MainScreen*/
        let {getCoordsFunc, getPolarCoordsFunc, radius} = this.props;

        /*состояние компонента, pan - координаты для анимации, visible - активность компонента, base64 - закодированное сообщение*/
        this.state = {
            pan: new Animated.ValueXY(),
            visible: false,
            base64: null
        };

        /*описание взаимодействия с жестами*/
        this._panResponder = PanResponder.create({

            /*возможность стать ответчиком жеста для компонента до касания*/
            onMoveShouldSetResponderCapture: () => true,

            /*возможность стать ответчиком жеста для компонента до каждого касания*/
            onMoveShouldSetPanResponderCapture: () => true,

            /*момент становления ответчиком*/
            onPanResponderGrant: (e, gestureState) => {
                this.state.pan.setValue({x: 0, y: 0});
                this.setState({visible: true});
            },

            /*момент перемещения*/
            onPanResponderMove: (evt, gestureState) => {
                let x = this.recalculateX(gestureState.dx, gestureState.dy);
                let y = this.recalculateY(gestureState.dx, gestureState.dy);

                getCoordsFunc(x / radius, -y / radius);
                getPolarCoordsFunc(this.getR(x, y) / radius, 2 * Math.PI - this.getPolarAlpha(x, y));
                this.state.pan.setValue({x: x, y: y});
            },

            /*момент окончания жеста*/
            onPanResponderRelease: (e, gestureState) => {
                getCoordsFunc(0, 0);
                getPolarCoordsFunc(0, 0);
                this.state.pan.setValue({x: 0, y: 0});
                this.setState({visible: false});
            }
        });
    }

    /*переопределнный метод при монтировании компонента*/
    componentDidMount() {
        let {radius, ip, port} = this.props;

        /*инициализация класса WebSocket*/
        let socket = new WebSocket('ws://' + ip + ':' + port);

        /*метод для отправки сообщения*/
        socket.onmessage = ({data}) => {
            let {getSourceFunc, pidIsEnable, mlIsEnable} = this.props;
            let pid = 0; let ml = 0;
            getSourceFunc(data);

            if(pidIsEnable)
                pid = 1;
            else
                pid = 0;

            if(mlIsEnable)
                ml = 1;
            else
                ml = 0;

            socket.send(this.state.pan.x._value / radius + ' ' + this.state.pan.y._value / -radius + ' ' + pid + ' ' + ml);
        };

        /*событие при открытии соединения и отправки нулевых данных*/
        socket.onopen = () => {
            socket.send(this.state.pan.x._value / radius + ' ' + this.state.pan.y._value / -radius + ' ' + 0 + ' ' + 0);
        }
    }

    /*метод для пересчета координаты X для ограничения перемещения малого компонента*/
    recalculateX(x, y) {
        let r = this.getR(x, y);
        if (r > this.props.radius) {
            let alpha = this.getAlpha(x, y);
            return this.props.radius * Math.cos(alpha);
        } else
            return x;
    }

    /*метод для пересчета координаты Y для ограничения перемещения малого компонента*/
    recalculateY(x, y) {
        let r = this.getR(x, y);
        if (r > this.props.radius) {
            let alpha = this.getAlpha(x, y);
            return this.props.radius * Math.sin(alpha);
        } else
            return y;
    }

    /*метод для получения полярного радиуса*/
    getR(x, y) {
        return Math.sqrt(x * x + y * y);
    }

    /*метод для получения угла*/
    getAlpha(x, y) {
        if (x > 0 && y < 0)
            return Math.atan(y / x);
        if (x > 0 && y > 0)
            return Math.atan(y / x) + 2 * Math.PI;
        if (x < 0)
            return Math.atan(y / x) + Math.PI;
        if (x === 0 && y < 0)
            return Math.PI / 2;
        if (x === 0 && y > 0)
            return 3 * Math.PI / 2;
        if (x === 0 && y === 0)
            return 0;
    }

    /*метод для получения полярного угла*/
    getPolarAlpha(x, y) {
        if (x > 0 && y > 0)
            return Math.atan(y / x);
        if (x > 0 && y < 0)
            return Math.atan(y / x) + 2 * Math.PI;
        if (x < 0)
            return Math.atan(y / x) + Math.PI;
        if (x === 0 && y > 0)
            return Math.PI / 2;
        if (x === 0 && y < 0)
            return 3 * Math.PI / 2;
        if (x === 0 && y === 0)
            return 0;
    }

    /*обязательный метод для рендеринга*/
    render() {
        let {radius} = this.props;
        let {pan, visible} = this.state;
        let [translateX, translateY] = [pan.x, pan.y];
        let imageStyle = {transform: [{translateX}, {translateY}]};
        return (
            <View style={[styles.externalNipple, {height: radius * 2, width: radius * 2, borderRadius: radius}]}>

                {/*компонент для осуществления анимации*/}
                <Animated.View style={imageStyle} {...this._panResponder.panHandlers}>

                    {/*малый контейнер*/}
                    <View
                        style={[styles.internalNipple,
                            {height: radius * 2 * 0.6, width: radius * 2 * 0.6, borderRadius: radius * 2 * 0.3},
                            visible ? {backgroundColor: 'rgba(255, 140, 0, 0.8)'} :
                                {backgroundColor: 'rgba(255, 140, 0, 0.6)'}]}/>
                </Animated.View>
            </View>
        )
    }
};

/*CSS-подобные стили для компонентов*/
const styles = StyleSheet.create({
    externalNipple: {
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    internalNipple: {
        overflow: 'hidden'
    },
    imageBlock: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: 'red'
    },
    image: {
        width: '100%',
        height: '100%'
    }
});
