/*импорт компонентов*/
import React, {Component} from 'react';
import {Animated, StyleSheet, View} from 'react-native';

import {PanGestureHandler, State} from 'react-native-gesture-handler';

export default class TankHandler extends Component {
    constructor(props) {
        super(props);
        /*получение значений параметров*/
        let {getCoordinatesFunc, type} = this.props;

        /*состояние компонента*/
        this.state = {
            visible: false
        };

        /*координаты x и y для анимации*/
        this._translateX = new Animated.Value(0);
        this._translateY = new Animated.Value(0);

        /*событие при изменении состоянии жеста*/
        this._onGestureEvent = Animated.event(
            [
                {
                    nativeEvent: {
                        translationX: 0,
                        translationY: this._translateY
                    },
                },
            ],
            {
                listener: (event) => {

                    /*перерасчет Y координаты компонента для ограничения перемещения малого компонента*/
                    event.nativeEvent.translationY = this.recalculateY(event.nativeEvent.translationY);

                    /*запись нового значения Y координаты*/
                    this._translateY.setValue(event.nativeEvent.translationY);
                    this.setState({visible: true});

                    /*передача координаты в компонент MainScreen*/
                    getCoordinatesFunc(type, this._translateY._value);
                }
            }
        );
    }

    /*метод для описания собития конца касания*/
    _onHandlerStateChange = event => {
        let {getCoordinatesFunc, type} = this.props;

        if (event.nativeEvent.oldState === State.ACTIVE) {
            /*установка координаты Y = 0 и передача ее компоненту MainScreen*/
            this._translateY.setValue(0);
            getCoordinatesFunc(type, this._translateY._value);
            this.setState({visible: false});
        }
    };

    /*метод для перерасчет координаты Y*/
    recalculateY(y) {
        y = Math.max(y, -100);
        y = Math.min(y, 100);
        return y;
    }

    /*обязательный метод для рендеринга*/
    render() {
        let {visible} = this.state;
        let {heightHandler} = this.props;
        return (
            <View style={{flex: 1}}>
                <View style={[styles.externalNipple, {height: heightHandler}]}>

                    {/*компонент для области собитий над жестом*/}
                    <PanGestureHandler
                        {...this.props}
                        maxPointers={1}
                        onGestureEvent={this._onGestureEvent}
                        onHandlerStateChange={this._onHandlerStateChange}
                    >
                        {/*компонент для анимирования*/}
                        <Animated.View
                            style={[
                                styles.internalNipple,
                                visible ? {
                                    backgroundColor: 'rgba(255, 140, 0, 0.8)'
                                } : {
                                    backgroundColor: 'rgba(255, 140, 0, 0.3)'
                                },
                                {
                                    transform: [
                                        {translateX: this._translateX},
                                        {translateY: this._translateY},
                                    ],
                                }
                            ]}
                        />
                    </PanGestureHandler>
                </View>
            </View>
        );
    }
}

/*CSS-подобные стили для компонентов*/
const styles = StyleSheet.create({
    externalNipple: {
        width: 30,
        borderRadius: 20,
        backgroundColor: 'rgba(255, 140, 0, 0.3)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    internalNipple: {
        height: 60,
        width: 60,
        borderRadius: 20,
        overflow: 'hidden'
    }
});