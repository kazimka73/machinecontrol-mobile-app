/*данные для отображения*/
export default {
    images: {
        checkBoxChecked: require('../images/checkbox_checked.png'),
        checkBoxUnchecked: require('../images/checkbox_unchecked.png')
    },
    settingsScreen: {
        buttonTitle: 'Начать',
        placeholderIP: 'IP',
        placeholderPort: 'port',
        errorIp: 'Неверный формат ip',
        errorPort: 'Неверный формат номера порта'
    },
    mainScreen: {
        titleCoordinates: 'Coordinates:',
        titleX: 'X:',
        titleY: 'Y:',
        checkBoxesTitle: [
            'ПИД',
            'МЛ'
        ]
    }
}