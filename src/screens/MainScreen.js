/*импорт компонентов*/
import React from 'react';
import {StyleSheet, View, Text, Dimensions} from 'react-native';

import Video from "../components/Video";
import CheckBox from "../components/CheckBox";
import TankHandler from "../components/TankHandler";
import Nipple from "../components/Nipple";
import constants from "../constants/constants";

/*инициализация констант*/
const heightHandler = 200;
/*получение высоты и ширины экрана устройства*/
const {width, height} = Dimensions.get('window');

export default class MainScreenOld extends React.Component {
    constructor(props) {
        super(props);

        /*состояние класса: isVisible - активность*/
        this.state = {
            /*x, y - координаты Nipple, r - полярный радиус, alpha - полярный угол*/
            x: 0,
            y: 0,
            r: 0,
            alpha: 0,

            /*base64 - закодированное изображение в формате base64*/
            base64: null,

            /*флаги МЛ и ПИД*/
            pidIsEnable: false,
            mlIsEnable: false,

            /*y координаты для левого и правого TankHandler*/
            yLeft: 0,
            yRight: 0
        }
    }

    /*переопределнный метод при монтирования компонента*/
    componentDidMount() {
        /*получение праметров ip и port, полученных при навигировании*/
        let ip = this.props.navigation.getParam('ip');
        let port = this.props.navigation.getParam('port');
        /*создание экземпляра класса WebSocket*/
        let socket = new WebSocket('ws://' + ip + ':' + port);

        /*метод описывающий получение данных, data - полученное сообщение*/
        socket.onmessage = ({data}) => {
            let {pidIsEnable, mlIsEnable} = this.state;
            let {yLeft, yRight} = this.state;
            let pid = 0; let ml = 0;

            /*запись в состояние полученного изображения в формате base64*/
            this.setState({base64: data});

            /*переопределение значений флагов МЛ и ПИД*/
            if(pidIsEnable)
                pid = 1;
            if(mlIsEnable)
                ml = 1;

            /*метод отправки сообщения*/
            socket.send(yLeft + ' ' + yRight + ' ' + pid + ' ' + ml);
        };

        /*метод описывающий открытие соединения и отправки нулевых данных*/
        socket.onopen = () => {
            socket.send(0 + ' ' + 0 + ' ' + 0 + ' ' + 0);
        }
    }

    /*метод для получения коодринат от компонента TankHandler, type - компонент (левый или правый), y - координата по оси Y*/
    getTankCoordinate(type, y) {
        if (type === 'left')
            this.setState({yLeft: this.recalculateY(y)});

        if (type === 'right') {
            this.setState({yRight: this.recalculateY(y)});
        }
    }

    /*метод для пересчета координаты Y, с нормализированием в отрезке [-1; 1]*/
    recalculateY(y, heightHandler) {
        return y / -100;
    }

    /*метод, изменяющий флаги МЛ и ПИД*/
    changeEnableStatus(type, value) {
        if (type === 'pid')
            this.setState({pidIsEnable: value});
        if (type === 'ml')
            this.setState({mlIsEnable: value});
    }

    /*обязательный метод для рендеринга компонентов*/
    render() {
        let {base64, pidIsEnable, mlIsEnable, yLeft, yRight} = this.state;
        console.log('width: ', width);
        return (
            <View style={styles.mainContainer}>

                {/*описание компонентов флагов МЛ и ПИД*/}
                <View style={{position: 'absolute', top: 10, right: 0, zIndex: 1000}}>
                    <CheckBox
                        text={constants.mainScreen.checkBoxesTitle[0]} type={'pid'} isChecked={pidIsEnable}
                        onPress={this.changeEnableStatus.bind(this)}/>
                    <CheckBox
                        text={constants.mainScreen.checkBoxesTitle[1]} type={'ml'} isChecked={mlIsEnable}
                        onPress={this.changeEnableStatus.bind(this)}/>
                </View>

                {/*описание компонента Video, для отображения видео с камеры*/}
                <Video base64={base64}/>

                {/*описание левого компонента TankHandler*/}
                <View style={styles.nippleLeft}>
                    <TankHandler
                        getCoordinatesFunc={this.getTankCoordinate.bind(this)}
                        type={'left'}
                        heightHandler={width < 800 ? heightHandler : heightHandler * 1.3}
                    />
                </View>

                {/*описание правого компонента TankHandler*/}
                <View style={styles.nippleRight}>
                    <TankHandler
                        getCoordinatesFunc={this.getTankCoordinate.bind(this)}
                        type={'right'}
                        heightHandler={width < 800 ? heightHandler : heightHandler * 1.3}
                    />
                </View>

                {/*описания компонента Nipple*/}
                {/*<View style={styles.nippleRight}>*/}
                {/*    <Nipple/>*/}
                {/*</View>*/}

                {/*описание для вывода координат компонентов TankHandler*/}
                <View style={{position: 'absolute', top: 10, left: 10}}>
                    <Text style={styles.textInfo}>{constants.mainScreen.titleCoordinates}</Text>
                    <Text style={styles.textInfo}>{constants.mainScreen.titleX} {yLeft.toFixed(2)};</Text>
                    <Text style={styles.textInfo}>{constants.mainScreen.titleY} {yRight.toFixed(2)};</Text>
                </View>
            </View>
        )
    }
}

/*CSS-подбные стили для элементов*/
const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#b0bec5',
        flex: 1
    },
    battery: {
        position: 'absolute',
        top: 10,
        right: 20
    },
    checkBoxes: {
        position: 'absolute',
        bottom: 0,
        left: 0
    },
    nippleRight: {
        position: 'absolute',
        bottom: 60,
        right: 40
    },
    nippleLeft: {
        position: 'absolute',
        bottom: 60,
        left: 40
    },
    textInfo: {
        color: 'rgba(255, 140, 0, 1)',
        fontWeight: 'bold'
    }
});
