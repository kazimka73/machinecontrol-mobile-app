/*импорт компонентов*/
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, TextInput} from 'react-native';

import Icon from "react-native-vector-icons/MaterialIcons";
import AsyncStorage from '@react-native-community/async-storage';
import constants from "../constants/constants";

/*инициализация констант*/
const rgbBlack = 'rgb(45, 53, 64)';
const iconSize = 25;

export default class SettingsScreen extends React.Component {
    constructor(props) {
        super(props);

        /*состояние класса*/
        this.state = {
            /*начальные данные инпутов ip и port*/
            ip: '192.168.1.',
            port: '9092',
            /*булевы переменные, отвечающие статус валидации ip и port*/
            errorIp: false,
            errorPort: false
        }
    }

    /*метод для изменения и валидации значения инпутов, type - указывает на поле у которого изменяется значение, value - текущее значение*/
    changeValue(type, value) {
        const regexIp = /^(?=.*[^\.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/;
        const regexPort = /^[0-9]{4}$/;

        if (type === 'ip') {
            this.setState({errorIp: !regexIp.test(value), ip: value});
            if (value === '')
                this.setState({errorIp: false});
        }
        if (type === 'port') {
            this.setState({errorPort: !regexPort.test(value), port: value});
            if (value === '')
                this.setState({errorIp: false});
        }
    }

    /*асинхронный метод для получения последних значений полей в локальном хранилище, если значение существует, то записывается в инпут,
    type - для определения ключа, значение которого нужно получить (ip или port)*/
    getLastValue = async (type) => {
        try {
            if (type === 'ip') {
                /*получение значения из локального хранилища по ключу @ip*/
                const value = await AsyncStorage.getItem('@ip');
                if (!!value)
                    this.changeValue('ip', value);
            }
            if (type === 'port') {
                /*получение значения из локального хранилища по ключу @port*/
                const value = await AsyncStorage.getItem('@port');
                if (!!value)
                    this.changeValue('port', value);
            }
        } catch (e) {}
    };

    /*метод для очистки, type - для определения инпута для очистки значения*/
    clearValue(type) {
        if (type === 'ip')
            this.setState({ip: ''});
        if (type === 'port')
            this.setState({port: ''});
    }

    /*асинхронный метод для занесения новых значений ip и port в локальное хранилище и последующей навигации на экран MainScreen с
    передачей параметров ip и port*/
    async perform(ip, port) {
        let {navigation} = this.props;

        /*запись новых значений по ключам @ip и @port в локальное хранилище*/
        await AsyncStorage.setItem('@ip', ip);
        await AsyncStorage.setItem('@port', port);

        /*навигирование на экран MainScreen, передача параметров ip и port*/
        navigation.navigate('MainScreen', {
            ip: ip,
            port: port
        });
    }

    /*обязательный метод для рендеринга компонентов*/
    render() {
        let {ip, port, errorIp, errorPort} = this.state;
        return (
            <View style={styles.mainContainer}>
                <View style={styles.centerContainer}>
                    <View style={{alignItems: 'center'}}>
                        <View style={styles.container}>
                            <TouchableOpacity style={styles.iconTouchableView} onPress={() => this.getLastValue('ip')}>
                                <Icon name={"replay"} size={iconSize} color={rgbBlack}/>
                            </TouchableOpacity>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => this.changeValue('ip', text)}
                                value={ip}
                                placeholder={constants.settingsScreen.placeholderIP}
                            />
                            <TouchableOpacity style={styles.iconTouchableView} onPress={() => this.clearValue('ip')}>
                                <Icon name={"clear"} size={iconSize} color={rgbBlack}/>
                            </TouchableOpacity>
                        </View>

                        {/*контейнер с текстом для описания ошибки для значения инпута ip*/}
                        <View style={{height: 10}}>
                            {(errorIp && ip !== '') && <Text style={styles.errorText}>{constants.settingsScreen.errorIp}</Text>}
                        </View>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <View style={styles.container}>
                            <TouchableOpacity
                                style={styles.iconTouchableView}
                                onPress={() => this.getLastValue('port')}
                            >
                                <Icon name={"replay"} size={iconSize} color={rgbBlack}/>
                            </TouchableOpacity>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => this.changeValue('port', text)}
                                value={port}
                                placeholder={constants.settingsScreen.placeholderPort}
                            />
                            <TouchableOpacity style={styles.iconTouchableView} onPress={() => this.clearValue('port')}>
                                <Icon name={"clear"} size={iconSize} color={rgbBlack}/>
                            </TouchableOpacity>
                        </View>

                        {/*контейнер с текстом для описания ошибки для значения инпута port*/}
                        <View style={{height: 10}}>
                            {(errorPort && port !== '') && <Text style={styles.errorText}>{constants.settingsScreen.errorPort}</Text>}
                        </View>
                    </View>

                    {/*компонент описывающий кнопку "Начать"*/}
                    <View style={[styles.container, {justifyContent: 'center'}]}>
                        {(!errorPort && !errorPort && ip !== '' && port !== '') ?
                            <TouchableOpacity
                                style={[styles.button, styles.buttonActive]}
                                onPress={async () => this.perform(ip, port)}
                            >
                                <Text>{constants.settingsScreen.buttonTitle}</Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity
                                style={[styles.button, styles.buttonInactive]}
                            >
                                <Text>{constants.settingsScreen.buttonTitle}</Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
        )
    }
}

/*CSS-подбные стили для элементов*/
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(245, 245, 245)'
    },
    centerContainer: {
        height: '60%',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    smallContainer: {
        alignItems: 'center'
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 280
    },
    button: {
        width: 200,
        height: 40,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',

    },
    buttonActive: {
        backgroundColor: 'rgba(255, 140, 0, 0.5)'
    },
    buttonInactive: {
        borderColor: 'rgb(153, 153, 153)',
        borderWidth: 1
    },
    iconTouchableView: {
        width: 40,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        height: 40,
        width: 200,
        borderColor: 'gray',
        borderRadius: 10,
        borderWidth: 1,
        textAlign: 'center'
    },
    buttonTitle: {
        color: 'gray'
    },
    errorText: {
        color: 'red',
        fontSize: 12
    }
});
