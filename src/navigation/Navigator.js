/*импорт компонентов*/
import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import MainScreen from '../screens/MainScreen';
import StatusBarCustom from '../components/StatusBarCustom';
import SettingsScreen from "../screens/SettingsScreen";

/*описаине стекового навигатора*/
const Main = createStackNavigator({
    /*экран настроек*/
    SettingsScreen: {
        screen: SettingsScreen,
        navigationOptions: ({}) => ({
            header: <StatusBarCustom/>
        })
    },
    /*главный компонент*/
    MainScreen: {
        screen: MainScreen,
        navigationOptions: ({}) => ({
            header: <StatusBarCustom/>
        })
    },
}, {
    /*дефолтный маршрут*/
    initialRouteName: 'SettingsScreen'
});

/*взязка навигатора и приложения*/
const AppContainer = createAppContainer(Main);
export default AppContainer;
